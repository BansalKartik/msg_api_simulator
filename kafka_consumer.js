const { Kafka } = require("kafkajs");
import constants from "./constants";
// import log from "./log"


const fs= require('fs');
const kafka = new Kafka({ brokers: constants.KAFKA_HOSTS, clientId: constants.clientId });

// log(`Kafka sessiontimeout: ${constants.KAFKA_SESSION_TIMEOUT}`, "DEBUG");
// log(`Kafka rebalanceTimeout: ${constants.KAFKA_RE_BALANCE_TIMEOUT}`, "DEBUG");
// log(`Kafka heartbeatInterval: ${constants.KAFKA_HEART_BEAT_INTERVAL}`, "DEBUG");

const consumer = kafka.consumer({
  groupId: "msg_api_events",
  // sessionTimeout: Number(constants.KAFKA_SESSION_TIMEOUT),
  // rebalanceTimeout: Number(constants.KAFKA_RE_BALANCE_TIMEOUT),
  // heartbeatInterval: Number(constants.KAFKA_HEART_BEAT_INTERVAL)
});

const run = async () => {
  await consumer.connect();
  // log(`Kafka consumer successfully connected...`, "DEBUG");
  await consumer.subscribe({ topic: "MRTM_RESPONSE_TOPIC", fromBeginning: true });

  //Consumer for the topic 
  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      try {
        const prefix = `${topic}[${partition} | ${message.offset}] / ${message.timestamp}`;
        // log(`- ${prefix} ${message.key}#${message.value}`, "DEBUG");
        if (message.value !== null && message.value !== undefined) {
        let val = JSON.parse(message.value.toString());  
          console.log(val)
        
        
        // fs.writeFile(`log_files/${new Date().getTime()}.json`, JSON.stringify(val), err => {
        //     if (err) {
        //         console.log(err);
        //     }
        
        //     }
        // );
         
        }
        await consumer.commitOffsets([{ topic, partition, offset: Number(message.offset) + 1 }]);
        // log(`Kafka committed offset for topic = '${topic}' partition = '${partition}' offset = '${Number(message.offset) + 1}'`, "DEBUG");
      } catch (kafkaException) {
        await consumer.commitOffsets([{ topic, partition, offset: Number(message.offset) + 1 }]);
        // log(`Kafka committed offset for topic = '${topic}' partition = '${partition}' offset = '${Number(message.offset) + 1}'`, "DEBUG");
        // log(`Error while parsing kafka message in kafka_consumer.js. Error Message: ${kafkaException.message}`, "ERROR");
      }
    }
  });


}

run()
  .catch(error => {
    log(`Error occured while connecting to kafka in kafka_consumer.js. Error Message: ${error.message}`, "ERROR");
  });

import constants from "./constants";
const { Kafka } = require("kafkajs");
import log from "./logger";

const kafka = new Kafka({
  clientId: constants.clientId,
  brokers: constants.KAFKA_HOSTS
});
const producer = kafka.producer();
producer.connect();

/**
 * @param  {String} topic
 * @param  {Object} component_data: with key-value format
 * @returns void
 * @example publishToKafka("my_topic", { key: 'value'})
 */
const publishToKafka = async (topic, component_data, key) => {
  try {
    if (key) {
      await producer.send({
        topic,
        messages: [
          {
            key: JSON.stringify(key),
            value: JSON.stringify(component_data)
          }
        ]
      });
    } else {
      await producer.send({
        topic,
        messages: [
          {
            value: JSON.stringify(component_data)
          }
        ]
      });
    }
  } catch (error) {
    log(`Error while publishing to kafka topic: '${topic}' value: ${JSON.stringify(component_data)}. Error Message: ${error.message}`,"ERROR");
  }
};

export default publishToKafka;

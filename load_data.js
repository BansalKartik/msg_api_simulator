
import publishToKafka from "./kafka_producer";

let emailRequest = {
	customer_id: 1,
	transaction_id: "1234",
	contact_id_type: "email",
	contact_id: "abcd@gmail.com",
	user_data: {
		requesting_system_id: "1234"
	},
	priority: 1,
	mergetags: {
		"First Name": "Pavan",
		"Dynamic Tag": "Dynamic Value",
	},
	template_id: 1,
	delivery_parameters: {
		sender_name: "Sender Name",
		sender_address: "sender@gmail.com",
		reply_to: "reply@gmail.com",
		subject: "Hello <[Dynamics Tag]> [First Names]"
	},
	channel: "email"
}



setTimeout(async () => {
    console.log("Pushing campaign job to topic");
    publishToKafka("MessageSendRequest", emailRequest);
}, 1000);

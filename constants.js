const CMS_HOST = process.env.CMS_HOST || "http://localhost:1337";

let constants = {
    LOG_LEVEL: process.env.LOG_LEVEL || "info",
    CASSANDRA_CLIENT_CONFIGUARTION: {
        contact_points: process.env.CASSANDRA_HOSTS || "localhost:9042",
        local_data_center: process.env.CASSANDRA_DATACENTER || "datacenter1",
        keyspace: process.env.CASSANDRA_KEYSPACE || "factoreal",
        username: process.env.CASSANDRA_USERNAME || "cassandra",
        password: process.env.CASSANDRA_PASSWORD || "cassandra",
    },
    PG_CLIENT_CONFIGUARTION: {
        user: process.env.PG_USER || "postgres",
        host: process.env.PG_HOST || "localhost",
        database: process.env.PG_DB || "postgres",
        password: process.env.PG_PASSWORD || "postgres",
        port: process.env.PG_PORT || 5432,
        ssl: process.env.SSL || false,
    },
    REDIS_CLIENT_CONFIGUARTION: {
        host: process.env.REDIS_HOST || "127.0.0.1",
        port: process.env.REDIS_PORT || 6379,
    },
    KAFKA_HOSTS: ["localhost:9092"],
    KAFKA_SESSION_TIMEOUT: process.env.KAFKA_SESSION_TIMEOUT || 30000,
    KAFKA_RE_BALANCE_TIMEOUT: process.env.KAFKA_RE_BALANCE_TIMEOUT || 60000,
    KAFKA_HEART_BEAT_INTERVAL: process.env.KAFKA_HEART_BEAT_INTERVAL || 3000,
    CASSANDRA_FAIL: "cassandra_fail",
    AUTH_API: `${CMS_HOST}/auth/local`,
    ADMIN_USER: process.env.ADMIN_USER || "",
    ADMIN_PASSWORD: process.env.ADMIN_PASSWORD || "",
    INCOMING_REQUESTS_TOPIC: "MessageSendRequest",
    MESSAGE_RESPONSE_TOPIC: "SendResponseMessage",
    BULK_REQUESTS_TOPIC: "BulkRequestsTopic",
    UNSUBSCRIBE_LINK: process.env.UNSUBSCRIBE_LINK || "https://d38ujozxngp8ds.cloudfront.net",
    JWT_SECRET: process.env.JWT_SECRET || "0094d67a-a71a-45fc-8521-7d0e37b0e89f",
    SENDGRID_API_KEY: "",
    SES_ACCESS_KEY: "",
    SES_SECRET_ACCESS_KEY: "",
    SES_REGION: "",
    CONFIG_SET: "",
    EMAIL_SENDER_TOPIC: "EMAIL_API_REQUEST_TOPIC",
    SUPPORTED_CHANNELS: ["email","mobilepush"],
    UNSUBSCRIBE_LINK_TEXT_COLOR: process.env.UNSUBSCRIBE_LINK_TEXT_COLOR || "#808080",
    APNS_SANDBOX: process.env.APNS_SANDBOX || false,
    BATCH_AUTHORIZATION: "5b31fabc2bcc1e585ff0abef918ba1d9",
    BATCH_URL: "https://api.batch.com/1.1",
    INCOMING_REQUESTS_NIFI_TOPIC: "MobileMessageSendRequestToNifi",
    BATCH_TTL: process.env.APNS_SANDBOX || 1209600,
    EMAIL_REGEX: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    PHONE_REGEX: /^[0-9]{8,13}$/,
    REDIS_EXPIRE_TIME: 1209600,    // 14 days in seconds
    MRTM_INCOMING_REQUESTS_TO_MOBILEPUSH_TOPIC: "MRTMIncomingRequestsToMobilePush",
    BIG_QUERY_INSERTION_TOPIC: "records_events_mrtm"
};

if (process.env.KAFKA_BROKER) {
    constants["KAFKA_HOSTS"] = process.env.KAFKA_BROKER.split(",");
}

export default constants;
